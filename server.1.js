const express = require('express');
const path = require('path');
const bodyParser = require('body-parser');
const cors = require('cors');
const passport = require('passport');
const mongoose = require('mongoose');
const config = require('./config/database');

const index = require('./routes/index');
const api = require('./routes/api/candidates');

//Connection To Database
mongoose.Promise = global.Promise;
mongoose.connect(config.database);

//on connection 
mongoose.connection.on('connected', () => {
    console.log('Connected database' + config.database);
});

//on error
mongoose.connection.on('error', (err) => {
    console.log('database error:' + err);
});

const app = express();


//Port Number
const port = 7777;

//CORS Middleware
app.use(cors());

//Set Static Folder (ANGULAR)
app.use(express.static(path.join(__dirname, 'client/dist')));


//Body Parser Middleware
app.use(bodyParser.urlencoded({
    extended: true
}));
app.use(bodyParser.json());


// app.use('/candidates', candidates);
app.use('/', index);
app.use('/candidates', api);

app.use('*', function (req, res) {
    res.redirect('/');
});


//Start Server
app.listen(port, () => {
    console.log('server run on port ' + port);
});