export class Candidate {

    _id: String;
    name: String;
    email: String;
    username: String;
    description: String;

}
