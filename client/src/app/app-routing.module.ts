import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { HomeComponent } from './components/home/home.component';
import { CandidatesSearchComponent } from './components/candidates-search/candidates-search.component';
import {CandidateCenterComponent} from './components/candidate-center/candidate-center.component';

const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'candidates', component: CandidateCenterComponent },
  { path: 'search', component: CandidatesSearchComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }
