import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';

import 'rxjs/add/operator/map';

@Injectable()
export class CandidateService {

  private _getUrl = '/api/candidates';
  
  constructor(private _http: Http) { }

  getCandidates() {
    return this._http.get(this._getUrl)
      .map((response: Response) => response.json());
  }

}
