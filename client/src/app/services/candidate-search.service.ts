import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions, Response } from '@angular/http';

import { Observable } from 'rxjs/Observable';
// import 'rxjs/add/operator/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';

import { Candidate } from "./../class/candidate";

@Injectable()
export class CandidateSearchService {

  // private _getUrl = "/api/candidates";
  baseUrl: string = "/api/candidates";

  constructor(private _http: Http) {


  }
  getCandidates(): Observable<Candidate[]> {
    // return this._http.get(this._getUrl)
    return this._http.get(this.baseUrl)
      .map((response: Response) => response.json())

    
  }

  // getCandidates(): Observable<Candidate[]> {
    // return this._http.get(this._getUrl)
  //   return this._http.get(this.baseUrl)
  //     .map((response: Response) => {
  //       let candidates = response.json();

  //     })
  //     .catch(this.handleError);
  // }


  getCandidate(id: string): Observable<Candidate[]> {
    return this._http.get(this.baseUrl + '/' + id)
      .map((response: Response) => response.json())
      // .catch(this.handleError);
  }

  // private handleError(error: any) {
  //   console.error('server error:', error);
  //   if (error instanceof Response) {
  //     let errMessage = '';
  //     try {
  //       errMessage = error.json().error;
  //     } catch (err) {
  //       errMessage = error.statusText;
  //     }
  //     return Observable.throw(errMessage);
  //     // Use the following instead if using lite-server
  //     //return Observable.throw(err.text() || 'backend server error');
  //   }
  //   return Observable.throw(error || 'Node.js server error');
  // }
}
