/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { CandidateSearchService } from './candidate-search.service';

describe('CandidateSearchService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [CandidateSearchService]
    });
  });

  it('should ...', inject([CandidateSearchService], (service: CandidateSearchService) => {
    expect(service).toBeTruthy();
  }));
});
