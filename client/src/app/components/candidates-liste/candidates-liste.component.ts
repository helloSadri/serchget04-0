import { Component, OnInit, EventEmitter } from '@angular/core';
import { Candidate } from "../../class/candidate";

@Component({
  selector: 'app-candidates-liste',
  templateUrl: './candidates-liste.component.html',
  styleUrls: ['./candidates-liste.component.css'],
  inputs: ['candidate'],
  outputs: ['SelectCandidate']
})
export class CandidatesListeComponent implements OnInit {

  public SelectCandidate = new EventEmitter();
  constructor() { }

  ngOnInit() {
  }

  onSelect(cand: Candidate) {
    this.SelectCandidate.emit(cand);
  }

}
