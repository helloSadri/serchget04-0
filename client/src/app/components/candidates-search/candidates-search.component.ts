import { Component, OnInit } from '@angular/core';

import { Candidate } from "../../class/candidate";

import { CandidateSearchService } from 'app/services/candidate-search.service';



@Component({
  selector: 'app-candidates-search',
  templateUrl: './candidates-search.component.html',
  styleUrls: ['./candidates-search.component.css'],
  providers: [CandidateSearchService]
})
export class CandidatesSearchComponent implements OnInit {


performSearch(searchTerm: HTMLInputElement): void {
    console.log(`User entered: ${searchTerm.value}`);
}


  candidates: Array<Candidate>;


  selectedCandidate: Candidate;

  constructor(private _CandidateSearchService: CandidateSearchService) { }

  ngOnInit() {
    this._CandidateSearchService.getCandidates()
      .subscribe(resCandidateData => this.candidates = resCandidateData);
  }
  getSelectCandidate(candidate: any) {
    this.selectedCandidate = candidate;
    console.log(this.selectedCandidate);
  }

}


