import { Component, OnInit } from '@angular/core';
import { Candidate } from "app/class/candidate";
import { CandidateService } from "app/services/candidate.service";

@Component({
  selector: 'app-candidate-center',
  templateUrl: './candidate-center.component.html',
  styleUrls: ['./candidate-center.component.css'],
  providers: [CandidateService]
})
export class CandidateCenterComponent implements OnInit {

  candidates: Array<Candidate>;


  // candidates: Candidate[] = [
  //   { "_id": "01", "name": "name01", "email": "email01", "username": "username01", "description": "" },
  //   { "_id": "02", "name": "name02", "email": "email02", "username": "username02", "description": "" },
  //   { "_id": "03", "name": "name03", "email": "email03", "username": "username03", "description": "" },
  //   { "_id": "04", "name": "name04", "email": "email04", "username": "username04", "description": "" }
  // ];


  selectedCandidate: Candidate;

  constructor(private _candidateService: CandidateService) { }

  ngOnInit() {
    this._candidateService.getCandidates()
      .subscribe(resCandidateData => this.candidates = resCandidateData);
  }
  onSelectCandidate(candidate: any) {
    this.selectedCandidate = candidate;
    console.log(this.selectedCandidate);
  }

}
