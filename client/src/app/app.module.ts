import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { AppRoutingModule } from './app-routing.module';

import { AppComponent } from './app.component';

import { CandidateSearchService } from './services/candidate-search.service';
import { CandidateService } from './services/candidate.service';

import { CandidatesSearchComponent } from './components/candidates-search/candidates-search.component';
import { HomeComponent } from './components/home/home.component';
import { CandidatesListeComponent } from './components/candidates-liste/candidates-liste.component';
import { CandidatesDetailComponent } from './components/candidates-detail/candidates-detail.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { CandidateCenterComponent } from './components/candidate-center/candidate-center.component';

@NgModule({
  declarations: [
    AppComponent,
    CandidatesSearchComponent,
    HomeComponent,
    CandidatesListeComponent,
    CandidatesDetailComponent,
    NavbarComponent,
    CandidateCenterComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    AppRoutingModule
  ],
  providers: [CandidateSearchService,CandidateService ],
  bootstrap: [AppComponent]
})
export class AppModule { }
